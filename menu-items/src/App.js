import react, { useState } from 'react';
import './App.css';
import items from './Data';
import Menu from './Menu';
import Categorie from './Categorie';

const allCategories = ['all', ...new Set(items.map((item) => item.category))]

function App() {
  const [menuItems, setMenuItems] = useState(items);
  const [Categories, setCategories] = useState(allCategories);

  const filterItems = (category) => {
    if(category === 'all'){
      setMenuItems(items);
      return;
    }
    const newItems = items.filter((item) => item.category === category)
    setMenuItems(newItems);
  }
  return (
    <main>
      <section className='menu section'>
        <div className='title'>
          <h2>Our Menu</h2>
          <div className='underline'></div>
        </div>
        <Categorie Categories={Categories} filterItems = {filterItems}/>
        <Menu items = {menuItems}/>
        
      </section>
    </main>
  );
}

export default App;
