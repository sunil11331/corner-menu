import React from 'react'
import './App.css';

const Categorie = ({Categories,filterItems}) => {
  return (
    <div className='btn-container'>
      {Categories.map((Category, index) => {
          return (
              <button
              type='button'
              className='filter-btn'
              key={index}
              onClick={() => filterItems(Category)}>
                  {Category}
              </button>
          )
      })}
    </div>
  )
}

export default Categorie
